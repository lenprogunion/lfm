use std::path::PathBuf;

use iced::{widget::{container, button, column, text, image}, Length};


use crate::{Message, themes, manager::{self, files::Type}};

pub fn new(apps: &Vec<manager::files::Files>) -> iced_aw::Grid<'static, Message, iced::Renderer> {
    apps
    .iter()
    .fold(iced_aw::grid!().strategy(iced_aw::Strategy::ColumnWidth(150.0)), |r_flatpak, file_content|{
        r_flatpak.push(
            container(
                file_info(&file_content)
                
            )
            .width(Length::Fixed(150.0))
            .height(Length::Fixed(180.0)) 
        )
    })
}

fn file_info(file: &manager::files::Files) -> iced::Element<'static, Message> {
    match &file.file_type {
        Type::Folder => {
            button(
                column!(
                    image("")
                    .height(50)
                    .width(50),
                    text(file.path.clone())
                    .height(50)
                    .width(100)
                    .style(iced::theme::Text::Color(iced::color!(44, 44, 44)))
                    .horizontal_alignment(iced::alignment::Horizontal::Center)  
                )
            )
            .style(iced::theme::Button::Custom(Box::new(themes::buttons::ElemButtonTheme)))
            .on_press(Message::OpenFolder(file.path.clone()))
            .into()
        },
        _ => {
            button(
                column!(
                    image("")
                    .height(50)
                    .width(50),
                    text(file.path.clone())
                    .height(50)
                    .width(100)
                    .style(iced::theme::Text::Color(iced::color!(44, 44, 44)))
                    .horizontal_alignment(iced::alignment::Horizontal::Center)  
                ) 
            )
            .style(iced::theme::Button::Custom(Box::new(themes::buttons::ElemButtonTheme)))
            .on_press(Message::OpenFile(file.clone()))
            .into()
        }
    }
    
}