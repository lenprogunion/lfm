use std::fs;

use super::lexer::{Token, lexer};

#[derive(Clone, Debug)]
pub struct DefaultApps{
    pub image: String,
    pub document: String,
    pub pdf: String,
    pub txt: String,
}

impl DefaultApps {
    fn default() -> DefaultApps {
        DefaultApps{
            image: String::new(),
            document: String::new(),
            pdf: String::new(),
            txt: String::new(),
        }
    }
}

pub fn desktop_parser(path: String) -> DefaultApps {
    let paths = fs::read_dir(&path).unwrap();
    let mut apps = DefaultApps::default();
    for path_entry in paths {
        if let Ok(entry) = path_entry{
            if let Some(extension) = entry.path().extension() {
                if extension == "desktop"{
                    let file = std::fs::read(entry.path());
                    let data_from_file = String::from_utf8(file.unwrap()).unwrap();
                    let tokens = lexer(data_from_file);
                    if !parse_tokens(tokens.clone()).image.is_empty() {
                        return parse_tokens(tokens);
                    }
                }
            }
        }
    }
    apps
}

fn parse_tokens(tokens: Vec<Token>) -> DefaultApps {
    let mut is_directive = true;
    let mut result = DefaultApps::default();
    let mut exec = "";
    let mut is_flatpak = false;
    for token in tokens.iter() {
        match token{
            Token::Tag { name, value } => {
                if name.is_empty() || value.is_empty() { continue; };
                match name.as_str() {
                    "Exec" => {
                        if !is_directive{ continue; }
                        exec = &value;
                    },
                    "Name" => {
                        if !is_directive{ continue; }
                    },
                    "Icon" => {
                        if !is_directive{ continue; }
                    },
                    "MimeType" => {
                        if !is_directive{ continue; }
                        if is_flatpak {continue;}
                        if value.contains("image") {
                            println!("Here");
                            result.image = exec.split(' ').collect::<Vec<&str>>()[0].to_string();
                        };
                    },
                    "X-Flatpak" => {
                        return DefaultApps::default();
                    },
                    _ => {}
                }
            },
            Token::Directive(name) => {
                if name != "Desktop Entry" {
                    is_directive = false;
                }
            }
        }
    }
    result 
}

