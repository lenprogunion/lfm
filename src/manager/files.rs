use std::fs::{OpenOptions, create_dir_all, read_dir, copy, remove_file, File};

use std::io::{BufReader, BufWriter,prelude::*, Error};
use std::path::{PathBuf, Path};
use std::env::home_dir;

#[derive(Debug, Clone)]
pub enum Type {
    Folder,
    Image,
    Txt,
    PDF,
    Doc,
    None
}

pub enum Toggle{    
    Hide,
    TitleBar,
    OnPanel
}

#[derive(Debug, Clone)]
pub struct Files {
    pub path: String,
    pub file_type: Type
}

impl Files {
    pub fn default() -> Files {
        Files {
            path: String::new(),
            file_type: Type::None
        }
    }
}

pub fn create_config_dir() -> PathBuf{
    let home = home_dir().unwrap();
    let path_to = home.join(".config/liv/");
    create_dir_all(path_to.clone()).unwrap();
    path_to
}

pub fn create_settings_file() -> PathBuf {
    let home = home_dir().unwrap();
    let path_to = home.join(".config/liv/").join("settings.ini");
    if File::open(path_to.clone()).is_err(){
        let _ = File::create(&path_to.clone());   
    }
    path_to
}

pub fn view_directory(path_from: &String) -> Vec<Files> {
    let mut result = Vec::<Files>::new();
    let paths = read_dir(Path::new(&path_from)).unwrap();

    for path in paths {
        let mut files = Files::default();
        if let Ok(entry) = path { 
            if entry.path().is_file() {
                let ext = match entry.path().extension() {
                    Some(e) => {
                        match e.to_str().unwrap() {
                            "png" | "jpeg" | "svg" | "jpg" => Type::Image,
                            "pdf" => Type::PDF, 
                            "doc" | "docx" => Type::Doc,
                            "txt" => Type::Txt,
                            _ => Type::None
                        }
                    },
                    None => Type::Txt
                };
                files.file_type = ext;
            } 

            if entry.path().is_dir() {
                files.file_type = Type::Folder
            }

            files.path = entry.path().file_name().unwrap().to_string_lossy().to_string();
            result.push(files);
        }
    }

    result
}

pub fn toggle_property(toggle: Toggle, path: PathBuf) -> Result<(), Error> {
    let toggle_elem = match toggle {
        Toggle::Hide => {
            "X-NoDisplay-iced"
        },
        Toggle::TitleBar => {
            "titlebar_hide"
        },
        Toggle::OnPanel => {
            "OnPanel"
        }
    };
    let mut file = OpenOptions::new().read(true).write(true).open(&path.clone()).unwrap();
    let reader = BufReader::new(&file);
    let mut lines = reader.lines().map(|l| l.unwrap()).collect::<Vec<String>>();
    for i in 0..lines.len() {
        if lines[i].contains(&format!("{}=false", toggle_elem)) {
            lines[i] = lines[i].replace(&format!("{}=false", toggle_elem), &format!("{}=true", toggle_elem));
            let file = File::create(&path.clone()).unwrap();
            let mut writer = BufWriter::new(&file);
            for line in &lines {
                writeln!(writer, "{}", line)?;
            }
            return Ok(());
        } 
        if lines[i].contains(&format!("{}=true", toggle_elem)){
            lines[i] = lines[i].replace(&format!("{}=true", toggle_elem), &format!("{}=false", toggle_elem));
            let file = File::create(&path.clone()).unwrap();
            let mut writer = BufWriter::new(&file);
            for line in &lines {
                writeln!(writer, "{}", line)?;
            }
            return Ok(());
        }
    }
    let mut file = OpenOptions::new().write(true).append(true).open(&path.clone()).unwrap();
    file.write_all(format!("{}=true\n", toggle_elem).as_bytes())?;
    Ok(())
}