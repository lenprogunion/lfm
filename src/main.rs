use std::path::PathBuf;

use iced::{self, 
    Settings, 
    Application, 
    Command,
    executor, 
    widget::{
        container, 
        column, row,
        toggler, scrollable
    }, 
    window, 
    Length, Renderer,
};
use iced_glow;
use iced_aw::{
    modal,
    floating_element::Anchor,
    floating_element
};

mod widgets;
mod themes;
mod manager;

use manager::{files::{create_settings_file, create_config_dir, self, Type, Files}, parser::{DefaultApps, self}};
struct Liv {
    previous_folders: Vec<String>,
    current_folder: String,
    current_content: Vec<files::Files>,
    settings: manager::settings_parser::Settings,
    path_to_settings_file: PathBuf,
    is_hidden: bool,
    default_apps: DefaultApps
}

#[derive(Debug, Clone)]
pub enum Message {
    Close,
    DragWindow,
    ToggleModal,
    ToggleTitlebar(bool),
    OpenFile(Files),
    OpenFolder(String)
}   

impl Application for Liv {
    type Executor = executor::Default;
    type Theme = iced::Theme;
    type Message = Message;
    type Flags = ();
    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        create_config_dir();
        let path_to_settings_file = create_settings_file();
        let current_folder = String::from("/home/rscasm");
        let current_content = manager::files::view_directory(&current_folder);
        let defalt_apps =  parser::desktop_parser("/home/rscasm/.config/iced-appmanager/apps".to_owned());
        println!("{:?}", defalt_apps);
        (   
            Liv {
                previous_folders: Vec::<String>::new(),
                current_folder: current_folder,
                current_content: current_content,
                settings: manager::settings_parser::settings_parser(path_to_settings_file.clone()),
                path_to_settings_file: path_to_settings_file,
                is_hidden: true,
                default_apps: defalt_apps 
            },
            Command::none()
        )
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::Close => {
                return window::close::<Message>();
            },
            Message::DragWindow => {
                return window::drag();
            },
            Message::ToggleModal => {
                self.is_hidden = !self.is_hidden;
            },
            Message::ToggleTitlebar(is_hide) => {
                self.settings.titlebar_hide = is_hide;
                
                let _ = manager::files::toggle_property(manager::files::Toggle::TitleBar, self.path_to_settings_file.clone());
            },
            Message::OpenFile(file) => {
                
                match file.file_type {
                    Type::Image => {
                        std::process::Command::new(&self.default_apps.image)
                        .arg(self.current_folder.clone() + "/" + &file.path)
                        .spawn().unwrap();
                    },
                    _ => {}
                };
            },
            Message::OpenFolder(path) => {
                
                self.previous_folders.push(self.current_folder.clone());
                self.current_folder.push('/');  
                self.current_folder.push_str(&path);
                self.current_content = manager::files::view_directory(&self.current_folder);
            }
        }
        Command::none()
    }

    fn view(&self) -> iced::Element<Message> {
        let modal_overlay = if self.is_hidden {
            None
        } else {
            Some(
                container(
                    column!(
                        row!(
                            toggler(String::from("Показывать скрытые"), self.settings.titlebar_hide, |b| Message::ToggleTitlebar(b))
                            .width(Length::Fill),
                        ),
                    )
                )
                .height(500.0)
                .width(600.0)
                .center_x()
                .center_y()
                .style(iced::theme::Container::Custom(Box::new(themes::containers::ContainerTheme)))
            )
        };
        let main = floating_element(
            column!(
                scrollable(
                    widgets::files::new(&self.current_content)
                )
                
                .width(Length::Fill)
                .height(Length::Fill)
            )
            .width(Length::Fill)
            .height(Length::Fill),
            widgets::titlebar::new()
        )
        .anchor(Anchor::North);
        container(
            modal(main, modal_overlay)
            .backdrop(Message::ToggleModal)
            .on_esc(Message::ToggleModal)
            .style(iced_aw::style::ModalStyles::custom(themes::modal::ModalTheme))
        )
        .width(Length::Fill)
        .height(Length::Fill)
        .style(iced::theme::Container::Custom(Box::new(themes::containers::ContainerTheme)))
        .into()
    }

    fn title(&self) -> String {
        String::from("Left image viewer")
    }

    fn style(&self) -> iced::theme::Application{
        iced::theme::Application::Custom(Box::new(crate::themes::application::ApplicationTheme))
    }
}

fn main() -> iced::Result {
    Liv::run(Settings {
        window: window::Settings {
            decorations: false,
            resizable: true,
            size: (500,300),
            transparent: true,
            ..window::Settings::default()
        },
        ..Settings::default()
    })
}
